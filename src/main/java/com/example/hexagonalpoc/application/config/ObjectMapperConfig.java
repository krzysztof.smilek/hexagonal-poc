package com.example.hexagonalpoc.application.config;

import static com.fasterxml.jackson.databind.DeserializationFeature.FAIL_ON_NULL_FOR_PRIMITIVES;
import static com.fasterxml.jackson.databind.DeserializationFeature.FAIL_ON_NUMBERS_FOR_ENUMS;
import static com.fasterxml.jackson.databind.DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES;
import static com.fasterxml.jackson.databind.DeserializationFeature.USE_BIG_DECIMAL_FOR_FLOATS;
import static com.fasterxml.jackson.databind.MapperFeature.ACCEPT_CASE_INSENSITIVE_ENUMS;

import org.springframework.boot.autoconfigure.jackson.Jackson2ObjectMapperBuilderCustomizer;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class ObjectMapperConfig {

  @Bean
  public Jackson2ObjectMapperBuilderCustomizer jacksonCustomizer() {
    return objectMapperBuilder ->
        objectMapperBuilder
            .featuresToDisable(FAIL_ON_UNKNOWN_PROPERTIES)
            .featuresToEnable(
                FAIL_ON_NULL_FOR_PRIMITIVES,
                FAIL_ON_NUMBERS_FOR_ENUMS,
                ACCEPT_CASE_INSENSITIVE_ENUMS,
                USE_BIG_DECIMAL_FOR_FLOATS
            )
            .findModulesViaServiceLoader(true);
  }
}
