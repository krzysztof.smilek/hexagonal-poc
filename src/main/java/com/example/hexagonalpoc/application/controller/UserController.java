package com.example.hexagonalpoc.application.controller;

import com.example.hexagonalpoc.application.controller.mapper.UserMapper;
import com.example.hexagonalpoc.application.controller.model.CreateUserRequest;
import com.example.hexagonalpoc.application.controller.model.UserResponse;
import com.example.hexagonalpoc.domain.user.CreateUserCommandHandler;
import com.example.hexagonalpoc.domain.user.GetUserQueryHandler;
import com.example.hexagonalpoc.domain.user.model.query.GetUserByIdQuery;
import java.util.UUID;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RequiredArgsConstructor
@RestController
@RequestMapping("/users")
public class UserController {

  private final UserMapper userMapper;
  private final CreateUserCommandHandler createUserCommandHandler;
  private final GetUserQueryHandler getUserQueryHandler;

  @PostMapping
  public UserResponse createUser(CreateUserRequest request) {
    var command = userMapper.toCommand(request);
    var createdUser = createUserCommandHandler.handle(command);
    return userMapper.toResponse(createdUser);
  }

  @GetMapping("/{userId}")
  public UserResponse getUserById(@PathVariable("userId") UUID userId) {
    var user = getUserQueryHandler.getUserById(new GetUserByIdQuery(userId));
    return userMapper.toResponse(user);
  }
}
