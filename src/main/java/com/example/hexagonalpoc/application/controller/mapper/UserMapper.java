package com.example.hexagonalpoc.application.controller.mapper;

import com.example.hexagonalpoc.application.controller.model.CreateUserRequest;
import com.example.hexagonalpoc.application.controller.model.UserResponse;
import com.example.hexagonalpoc.domain.user.model.User;
import com.example.hexagonalpoc.domain.user.model.command.CreateUserCommand;
import org.springframework.stereotype.Component;

@Component
public class UserMapper {

  public CreateUserCommand toCommand(CreateUserRequest request) {
    return new CreateUserCommand(request.firstName(), request.lastName());
  }

  public UserResponse toResponse(User user) {
    return new UserResponse(user.getId(), "%s %s".formatted(user.getFirstname(), user.getLastName()));
  }
}
