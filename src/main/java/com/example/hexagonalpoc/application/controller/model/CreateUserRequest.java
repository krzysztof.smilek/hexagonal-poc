package com.example.hexagonalpoc.application.controller.model;

public record CreateUserRequest(String firstName, String lastName) {

}
