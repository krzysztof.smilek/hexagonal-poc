package com.example.hexagonalpoc.application.controller.model;

import java.util.UUID;

public record UserResponse(UUID id, String fullName) {

}
