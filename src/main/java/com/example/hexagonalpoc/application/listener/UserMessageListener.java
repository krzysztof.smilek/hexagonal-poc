package com.example.hexagonalpoc.application.listener;

import com.example.hexagonalpoc.application.listener.model.UserHasBeenBlockedEvent;
import com.example.hexagonalpoc.domain.user.DeactivateUserCommandHandler;
import com.example.hexagonalpoc.domain.user.model.command.DeactivateUserCommand;
import io.awspring.cloud.messaging.listener.annotation.SqsListener;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;

@Slf4j
@RequiredArgsConstructor
@Component
public class UserMessageListener {

  private final DeactivateUserCommandHandler deactivateUserCommandHandler;

  @SqsListener("users-to-deactivate")
  public void handleMessage(UserHasBeenBlockedEvent event) {
    log.info("Handling message {}", event);
    var command = toCommand(event);
    deactivateUserCommandHandler.handle(command);
  }

  private DeactivateUserCommand toCommand(UserHasBeenBlockedEvent event) {
    return new DeactivateUserCommand(event.userId());
  }
}
