package com.example.hexagonalpoc.application.listener.model;

import java.util.UUID;

public record UserHasBeenBlockedEvent(UUID userId) {

}
