package com.example.hexagonalpoc.domain.event;

public interface EventPublisher<EVENT> {

  void publish(EVENT event);
}
