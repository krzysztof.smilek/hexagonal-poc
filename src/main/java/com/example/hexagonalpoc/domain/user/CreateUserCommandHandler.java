package com.example.hexagonalpoc.domain.user;

import com.example.hexagonalpoc.domain.event.EventPublisher;
import com.example.hexagonalpoc.domain.user.model.User;
import com.example.hexagonalpoc.domain.user.model.command.CreateUserCommand;
import com.example.hexagonalpoc.domain.user.model.event.UserCreatedEvent;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

@RequiredArgsConstructor
@Service
public class CreateUserCommandHandler {

  private final UserRepository userRepository;
  private final EventPublisher<UserCreatedEvent> eventPublisher;

  public User handle(CreateUserCommand command) {
    var user = toDomainUser(command);
    var savedUser = userRepository.save(user);
    eventPublisher.publish(toEvent(savedUser));
    return savedUser;
  }

  private User toDomainUser(CreateUserCommand command) {
    return new User(null, command.firstname(), command.lastName());
  }

  private UserCreatedEvent toEvent(User savedUser) {
    return UserCreatedEvent.builder()
        .userId(savedUser.getId())
        .firstName(savedUser.getFirstname())
        .lastName(savedUser.getLastName())
        .build();
  }
}
