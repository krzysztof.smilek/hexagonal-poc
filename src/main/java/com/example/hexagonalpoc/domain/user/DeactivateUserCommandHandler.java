package com.example.hexagonalpoc.domain.user;

import com.example.hexagonalpoc.domain.user.model.User;
import com.example.hexagonalpoc.domain.user.model.command.DeactivateUserCommand;
import com.example.hexagonalpoc.domain.user.model.event.UserDeactivatedEvent;
import com.example.hexagonalpoc.infrastructure.broker.UserDeactivatedEventPublisher;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Component;

@RequiredArgsConstructor
@Component
public class DeactivateUserCommandHandler {

  private final UserRepository userRepository;
  private final UserDeactivatedEventPublisher eventPublisher;

  public void handle(DeactivateUserCommand command) {
    var userToDeactivate = userRepository.findById(command.id());
    userToDeactivate.deactivate();
    userRepository.save(userToDeactivate);
    eventPublisher.publish(toEvent(userToDeactivate));
  }

  private UserDeactivatedEvent toEvent(User user) {
    return UserDeactivatedEvent.builder()
        .userId(user.getId())
        .build();
  }
}
