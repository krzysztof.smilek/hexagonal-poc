package com.example.hexagonalpoc.domain.user;

import com.example.hexagonalpoc.domain.user.model.User;
import com.example.hexagonalpoc.domain.user.model.query.GetUserByIdQuery;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Component;

@RequiredArgsConstructor
@Component
public class GetUserQueryHandler {

  private final UserRepository userRepository;

  public User getUserById(GetUserByIdQuery query) {
    return userRepository.findById(query.id());
  }
}
