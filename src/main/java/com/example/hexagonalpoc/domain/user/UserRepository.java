package com.example.hexagonalpoc.domain.user;

import com.example.hexagonalpoc.domain.user.model.User;
import java.util.UUID;

public interface UserRepository {

  User save(User user);

  User findById(UUID id);
}
