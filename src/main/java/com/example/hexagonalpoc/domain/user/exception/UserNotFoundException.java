package com.example.hexagonalpoc.domain.user.exception;

import java.util.UUID;

public class UserNotFoundException extends RuntimeException {

  public UserNotFoundException(UUID id) {
    super("User with id=%s has not been found".formatted(id));
  }
}
