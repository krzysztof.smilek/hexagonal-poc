package com.example.hexagonalpoc.domain.user.model;

import java.util.UUID;
import lombok.Getter;
import lombok.RequiredArgsConstructor;

@RequiredArgsConstructor
@Getter
public class User {

  private final UUID id;
  private final String firstname;
  private final String lastName;
  private boolean isActive = true;

  public void deactivate() {
    isActive = false;
  }
}
