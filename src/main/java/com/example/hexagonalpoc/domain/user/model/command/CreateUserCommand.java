package com.example.hexagonalpoc.domain.user.model.command;

public record CreateUserCommand(String firstname, String lastName) {

}
