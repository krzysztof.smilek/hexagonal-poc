package com.example.hexagonalpoc.domain.user.model.command;

import java.util.UUID;

public record DeactivateUserCommand(UUID id) {

}
