package com.example.hexagonalpoc.domain.user.model.event;

import java.time.ZonedDateTime;
import java.util.UUID;
import lombok.Builder;
import lombok.Data;

@Data
@Builder
public class UserDeactivatedEvent {

  private final UUID id = UUID.randomUUID();
  private final ZonedDateTime timestamp = ZonedDateTime.now();

  private final UUID userId;
}
