package com.example.hexagonalpoc.domain.user.model.query;

import java.util.UUID;

public record GetUserByIdQuery(UUID id) {

}
