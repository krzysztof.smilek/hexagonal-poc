package com.example.hexagonalpoc.infrastructure.broker;

import com.example.hexagonalpoc.domain.event.EventPublisher;
import lombok.Getter;
import lombok.RequiredArgsConstructor;
import org.springframework.cloud.stream.function.StreamBridge;

@RequiredArgsConstructor
public abstract class SpringCloudStreamPublisher<EVENT> implements EventPublisher<EVENT> {

  @Getter
  private final String topicName;
  private final StreamBridge streamBridge;

  @Override
  public void publish(EVENT event) {
    streamBridge.send(topicName, event);
  }
}
