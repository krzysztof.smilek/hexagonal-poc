package com.example.hexagonalpoc.infrastructure.broker;

import com.example.hexagonalpoc.domain.user.model.event.UserCreatedEvent;
import org.springframework.cloud.stream.function.StreamBridge;
import org.springframework.stereotype.Component;

@Component
public class UserCreatedEventPublisher extends SpringCloudStreamPublisher<UserCreatedEvent> {

  public UserCreatedEventPublisher(StreamBridge streamBridge) {
    super("new-users", streamBridge);
  }
}
