package com.example.hexagonalpoc.infrastructure.broker;

import com.example.hexagonalpoc.domain.user.model.event.UserDeactivatedEvent;
import org.springframework.cloud.stream.function.StreamBridge;
import org.springframework.stereotype.Component;

@Component
public class UserDeactivatedEventPublisher extends SpringCloudStreamPublisher<UserDeactivatedEvent> {

  public UserDeactivatedEventPublisher(StreamBridge streamBridge) {
    super("deactivated-users", streamBridge);
  }
}
