package com.example.hexagonalpoc.infrastructure.repository;

import com.example.hexagonalpoc.infrastructure.repository.model.UserEntity;
import java.util.UUID;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface MysqlUserRepository extends CrudRepository<UserEntity, UUID> {

}
