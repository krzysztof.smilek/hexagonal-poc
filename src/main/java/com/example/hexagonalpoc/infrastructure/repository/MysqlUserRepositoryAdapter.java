package com.example.hexagonalpoc.infrastructure.repository;

import com.example.hexagonalpoc.domain.user.UserRepository;
import com.example.hexagonalpoc.domain.user.exception.UserNotFoundException;
import com.example.hexagonalpoc.domain.user.model.User;
import com.example.hexagonalpoc.infrastructure.repository.model.UserEntity;
import java.util.UUID;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Component;

@RequiredArgsConstructor
@Component
public class MysqlUserRepositoryAdapter implements UserRepository {

  private final MysqlUserRepository repository;

  @Override
  public User save(User user) {
    var entity = toEntity(user);
    var savedEntity = repository.save(entity);
    return toDomain(savedEntity);
  }

  @Override
  public User findById(UUID id) {
    return repository.findById(id)
        .map(this::toDomain)
        .orElseThrow(() -> new UserNotFoundException(id));
  }

  public UserEntity toEntity(User user) {
    return UserEntity.builder()
        .id(user.getId())
        .firstName(user.getFirstname())
        .lastName(user.getLastName())
        .build();
  }

  private User toDomain(UserEntity userEntity) {
    return new User(userEntity.getId(), userEntity.getFirstName(), userEntity.getLastName());
  }
}
